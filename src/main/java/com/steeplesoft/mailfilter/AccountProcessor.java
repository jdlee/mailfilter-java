/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter;

import com.steeplesoft.mailfilter.model.Account;
import com.steeplesoft.mailfilter.model.Rule;
import com.steeplesoft.mailfilter.model.RuleComparator;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FromStringTerm;
import javax.mail.search.FromTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SentDateTerm;
import javax.mail.search.SubjectTerm;

/**
 *
 * @author jdlee
 */
public class AccountProcessor {

    final private Account account;
    final private Map<String, Folder> folders = new HashMap<>();
    private int deleteCount;
    private int moveCount;
    private Store store;

    public AccountProcessor(Account account) throws MessagingException {
        this.account = account;
    }
    
    public void process() throws MessagingException {
        Properties props = System.getProperties();
        props.setProperty("mail.imap.starttls.enable", "true");
        Session session = Session.getInstance(props, null);
        try {
            Map<String, List<Rule>> folderRules = getRulesByFolder(account.getRules());
            store = session.getStore("imaps");
            store.connect(account.getServerName(), account.getUserName(), account.getPassword());
            
            for (Map.Entry<String, List<Rule>> entry : folderRules.entrySet()) {
                processFolder(entry.getKey(), entry.getValue());
            }
        } finally {
            closeFolders();
            if (store != null) {
                store.close();
            }
        }
    }

    public int getDeleteCount() {
        return deleteCount;
    }

    public int getMoveCount() {
        return moveCount;
    }
    
    private void processFolder (String folder, List<Rule> rules) throws MessagingException {
        Folder source = getFolder(folder, Folder.READ_WRITE);
        
        for (Rule rule : rules) {
            if ("move".equals(rule.getType())) {
                if (rule.getMatchingText() != null) {
                    SearchTerm st = new FromStringTerm(rule.getMatchingText());
                    Message[] msgs = source.search(st);
                    if (msgs != null && msgs.length > 0) {
                        moveMessages(msgs, source, getFolder(rule.getDestFolder(), Folder.READ_WRITE));
                    }
                } else if (rule.getOlderThan() > 0) {
                    
                }
            } else  if ("delete".equals(rule.getType())) {
                if (rule.getMatchingText() != null) {
                    SearchTerm st = new FromStringTerm(rule.getMatchingText());
                    Message[] msgs = source.search(st);
                    if (msgs != null && msgs.length > 0) {
                        moveMessages(msgs, source, getFolder(rule.getDestFolder(), Folder.READ_WRITE));
                    }
                } else if (rule.getOlderThan() > 0) {
                    LocalDateTime day = LocalDateTime.now().minusDays(rule.getOlderThan());
                    SentDateTerm term = new SentDateTerm(ComparisonTerm.LE, 
                            Date.from(day.toLocalDate().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
                    Message[] msgs = source.search(term);
                    if (msgs != null && msgs.length > 0) {
                        deleteMessages(msgs, source);
                    }
                }
            }
        }
//        for (Message msg : source.getMessages()) {
//            processMessage(msg, rules);
//        }
    }
    
    private Map<String, List<Rule>> sortRules(List<Rule> rules) {
        Map<String, List<Rule>> sorted = new HashMap<>();

        for (Rule rule : rules) {
            String type = rule.getType();
            List<Rule> list = sorted.get(type);
            if (list == null) {
                list = new ArrayList<>();
                sorted.put(type, list);
            }
            list.add(rule);
        }

        return sorted;
    }
    
    private Map<String, List<Rule>> getRulesByFolder(List<Rule> rules) {
        Map<String, List<Rule>> map = new HashMap<>();
        
        for (Rule rule : rules) {
            List<Rule> list = map.get(rule.getSourceFolder());
            if (list == null) {
                list = new ArrayList<>();
                map.put(rule.getSourceFolder(), list);
            }
            list.add(rule);
        }
        
        return map;
    }

    private void deleteMessages(Message[] toDelete, Folder source) throws MessagingException {
        deleteCount += toDelete.length;
        source.setFlags(toDelete, new Flags(Flags.Flag.DELETED), true);
    }

    private void moveMessages(Message[] toMove, Folder source, Folder dest) throws MessagingException {
        moveCount += toMove.length;
        source.setFlags(toMove, new Flags(Flags.Flag.DELETED), true);
        source.copyMessages(toMove, dest);
    }

    private Folder getFolder(String folderName, int mode) throws MessagingException {
        Folder source;
        if (folders.containsKey(folderName)) {
            source = folders.get(folderName);
        } else {
            source = store.getFolder(folderName);
            folders.put(folderName, source);
            if (source == null || !source.exists()) {
                System.out.println("Invalid folder: " + folderName);
                System.exit(1);
            } else {
            }
        }
        if (mode > 0 && !source.isOpen()) {
            source.open(mode);
        }
        return source;
    }

    private void closeFolders() {
        for (Folder folder : folders.values()) {
            if (folder.isOpen()) {
                try {
                    folder.close(true);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
