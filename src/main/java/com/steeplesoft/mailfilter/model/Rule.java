/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jdlee
 */
public class Rule {
    private String type = "move";
    private String sourceFolder = "INBOX";
    private String destFolder;
    private List<String> fields = new ArrayList<>();
    private String matchingText;
    private int olderThan = -1;
    private boolean isRegex = false;

    public Rule() {
        fields.add("from");
    }

    public static Rule create() {
        return new Rule();
    }

    public Rule type(String type) {
        setType(type);
        return this;
    }

    public Rule sourceFolder(String name) {
        setSourceFolder(name);
        return this;
    }

    public Rule destFolder(String name) {
        setDestFolder(name);
        return this;
    }

    public Rule fields(List<String> fields) {
        setFields(fields);
        return this;
    }

    public Rule matchingText(String text) {
        setMatchingText(text);
        return this;
    }

    public Rule olderThan(int days) {
        setOlderThan(days);
        return this;
    }

    public Rule regex(boolean isRegex) {
        setIsRegex(isRegex);
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSourceFolder() {
        return sourceFolder;
    }

    public void setSourceFolder(String sourceFolder) {
        this.sourceFolder = sourceFolder;
    }

    public String getDestFolder() {
        return destFolder;
    }

    public void setDestFolder(String destFolder) {
        this.destFolder = destFolder;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public String getMatchingText() {
        return matchingText;
    }

    public void setMatchingText(String matchingText) {
        this.matchingText = matchingText;
    }

    public int getOlderThan() {
        return olderThan;
    }

    public void setOlderThan(int olderThan) {
        this.olderThan = olderThan;
    }

    public boolean getIsRegex() {
        return isRegex;
    }

    public void setIsRegex(boolean isRegex) {
        this.isRegex = isRegex;
    }

    @Override
    public String toString() {
        return "Rule{" + "type=" + type + ", sourceFolder=" + sourceFolder + ", destFolder=" + destFolder + ", matchingText=" + matchingText + ", olderThan=" + olderThan + "}\n";
    }
}
