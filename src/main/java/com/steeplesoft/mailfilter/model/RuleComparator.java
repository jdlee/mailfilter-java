/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.steeplesoft.mailfilter.model;

import java.util.Comparator;

/**
 *
 * @author jdlee
 */
public class RuleComparator implements Comparator<Rule> {

    @Override
    public int compare(Rule r1, Rule r2) {
        final String type1 = r1.getType();
        final String type2 = r2.getType();
        final String sourceFolder1 = r1.getSourceFolder();
        final String sourceFolder2 = r2.getSourceFolder();
        final String destFolder1 = r1.getDestFolder();
        final String destFolder2 = r2.getDestFolder();

        if (!type1.equals(type2)) {
            return type1.compareTo(type2);
        } else if (!sourceFolder1.equals(sourceFolder2)) {
            return sourceFolder1.compareTo(sourceFolder2);
        } else if (!destFolder1.equals(destFolder2)) {
            return destFolder1.compareTo(destFolder2);
        }

        return 0;
    }

}
