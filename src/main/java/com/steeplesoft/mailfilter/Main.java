package com.steeplesoft.mailfilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.steeplesoft.mailfilter.model.Account;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;

public class Main {

    final private ObjectMapper mapper = new ObjectMapper();
    private Account[] accounts;

    /**
     * @param args the command line arguments
     */
    public static void main(String... args) {
        new Main().run(args);
    }

    public void run(String... args) {
        try {
            String rulesFile = System.getProperty("user.home") + File.separatorChar + 
                    ".mailfilter" + File.separatorChar + "rules.json";
            if (args.length == 1) {
                rulesFile = args[0];
            }

            if (!new File(rulesFile).exists()) {
                System.err.println("The specified rules file does not exist: " + rulesFile);
                System.exit(-1);
            }

            accounts = mapper.readValue(new File(rulesFile), Account[].class);

            for (Account account : accounts) {
                AccountProcessor processor = new AccountProcessor(account);
                processor.process();
                System.out.println("Processing of account " + account.getUserName() + " completed.");
                System.out.println("\tDeleted count: " + processor.getDeleteCount());
                System.out.println("\tMove count:    " + processor.getMoveCount());
            }
        } catch (IOException | MessagingException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
